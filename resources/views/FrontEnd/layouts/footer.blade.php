<div class="footer-section section section-fluid-240" data-bg-color="#1C1C1C">
            <div class="container">

                <!-- Footer Top Widgets Start -->
                <div class="row mb-lg-14 mb-md-10 mb-6 align-items-center">

                    <!-- Footer Widget Start -->
                    <div class="col-lg-3 col-md-5 col-sm-12 col-12 col-12 mb-6">
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="index.html"><img src="frontend_style/images/logo/footer-logo.png" alt="Logo"></a>
                            </div>
                            <div class="footer-widget-content">
                                <div class="footer-social-inline">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-linkedin"></i></a>
                                    <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget End -->

                    <!-- Footer Widget Start -->
                    <div class="offset-lg-1 col-lg-5 col-md-7 col-sm-12 col-12 mb-6">
                        <div class="footer-widget">
                            <div class="footer-widget-content">
                                <ul class="column-2">
                                    <li><a href="#">Hastheme for Business</a></li>
                                    <li><a href="#">Teach on Udemy</a></li>
                                    <li><a href="#">Get the app</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Contact us</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Help and Support</a></li>
                                    <li><a href="#">Affiliate</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget End -->

                    <!-- Footer Widget Start -->
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12 mb-6">
                        <div class="footer-widget">
                            <div class="footer-widget-content">
                                <div class="ft-instagram-list">

                                    <div class="instagram-grid-wrap">

                                        <!-- Start Single Instagram -->
                                        <div class="item-grid grid-style--1">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="frontend_style/images/instagram/instagram1.png"
                                                        alt="instagram images">
                                                </a>
                                                <div class="item-info">
                                                    <div class="inner">
                                                        <a href="#"><i class="fas fa-heart"></i>1k</a>
                                                        <a href="#"><i class="fas fa-comment-dots"></i>9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Single Instagram -->

                                        <!-- Start Single Instagram -->
                                        <div class="item-grid grid-style--1">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="frontend_style/images/instagram/instagram2.png"
                                                        alt="instagram images">
                                                </a>
                                                <div class="item-info">
                                                    <div class="inner">
                                                        <a href="#"><i class="fas fa-heart"></i>1k</a>
                                                        <a href="#"><i class="fas fa-comment-dots"></i>9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Single Instagram -->

                                        <!-- Start Single Instagram -->
                                        <div class="item-grid grid-style--1">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="frontend_style/images/instagram/instagram3.png"
                                                        alt="instagram images">
                                                </a>
                                                <div class="item-info">
                                                    <div class="inner">
                                                        <a href="#"><i class="fas fa-heart"></i>1k</a>
                                                        <a href="#"><i class="fas fa-comment-dots"></i>9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Single Instagram -->

                                        <!-- Start Single Instagram -->
                                        <div class="item-grid grid-style--1">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="frontend_style/images/instagram/instagram4.png"
                                                        alt="instagram images">
                                                </a>
                                                <div class="item-info">
                                                    <div class="inner">
                                                        <a href="#"><i class="fas fa-heart"></i>1k</a>
                                                        <a href="#"><i class="fas fa-comment-dots"></i>9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Single Instagram -->

                                        <!-- Start Single Instagram -->
                                        <div class="item-grid grid-style--1">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="frontend_style/images/instagram/instagram5.png"
                                                        alt="instagram images">
                                                </a>
                                                <div class="item-info">
                                                    <div class="inner">
                                                        <a href="#"><i class="fas fa-heart"></i>1k</a>
                                                        <a href="#"><i class="fas fa-comment-dots"></i>9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Single Instagram -->

                                        <!-- Start Single Instagram -->
                                        <div class="item-grid grid-style--1">
                                            <div class="thumb">
                                                <a href="#">
                                                    <img src="frontend_style/images/instagram/instagram6.png"
                                                        alt="instagram images">
                                                </a>
                                                <div class="item-info">
                                                    <div class="inner">
                                                        <a href="#"><i class="fas fa-heart"></i>1k</a>
                                                        <a href="#"><i class="fas fa-comment-dots"></i>9</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start Single Instagram -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget End -->

                </div>
                <!-- Footer Top Widgets End -->

                <!-- Footer Copyright Start -->
                <div class="row">
                    <div class="col">
                        <p class="copyright">Copyright &copy; 2021 All Rights Reserved | Made with <i
                                class="fal fa-heart"></i> by <a href="https://hasthemes.com/">HasThemes</a>. </p>
                    </div>
                </div>
                <!-- Footer Copyright End -->

            </div>
        </div>

        <!-- Scroll Top Start -->
        <a href="#" class="scroll-top" id="scroll-top">
            <i class="arrow-top fal fa-long-arrow-up"></i>
            <i class="arrow-bottom fal fa-long-arrow-up"></i>
        </a>
        <!-- Scroll Top End -->

    </div>
    <div id="site-main-mobile-menu" class="site-main-mobile-menu">
        <div class="site-main-mobile-menu-inner">
            <div class="mobile-menu-header">
                <div class="mobile-menu-logo">
                    <a href="index.html"><img src="frontend_style/images/logo/logo.png" alt=""></a>
                </div>
                <div class="mobile-menu-close">
                    <button class="toggle">
                        <i class="icon-top"></i>
                        <i class="icon-bottom"></i>
                    </button>
                </div>
            </div>
            <div class="mobile-menu-content">
                <nav class="site-mobile-menu">
                    <ul>
                        <li>
                            <a href="index.html"><span class="menu-text">Home</span></a>
                        </li>
                        <li class="has-children">
                            <a href="about.html"><span class="menu-text">Pages</span></a>
                            <span class="menu-toggle"><i class="far fa-angle-down"></i></span>
                            <ul class="sub-menu">
                                <li><a href="about.html"><span class="menu-text">About</span></a></li>
                                <li><a href="team.html"><span class="menu-text">Our Team</span></a></li>
                                <li><a href="contact.html"><span class="menu-text">Contact</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="our-program.html"><span class="menu-text">Our Program</span></a>
                        </li>
                        <li class="has-children">
                            <a href="shop.html"><span class="menu-text">Shop</span></a>
                            <span class="menu-toggle"><i class="far fa-angle-down"></i></span>
                            <ul class="sub-menu">
                                <li><a href="shop.html"><span class="menu-text">Shop</span></a></li>
                                <li><a href="product-details.html"><span class="menu-text">Product Details</span></a>
                                </li>
                                <li><a href="shopping-cart.html"><span class="menu-text">Shopping Cart</span></a></li>
                                <li><a href="wishlist.html"><span class="menu-text">Wishlist</span></a></li>
                                <li><a href="checkout.html"><span class="menu-text">Checkout</span></a></li>
                            </ul>
                        </li>
                        <li class="has-children">
                            <a href="blog-grid.html"><span class="menu-text">Blog</span></a>
                            <span class="menu-toggle"><i class="far fa-angle-down"></i></span>
                            <ul class="sub-menu">
                                <li><a href="blog-grid.html"><span class="menu-text">Blog Grid</span></a></li>
                                <li><a href="blog-details.html"><span class="menu-text">Blog Details</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="event.html"><span class="menu-text">Event</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- JS
    --->
    <!-- Vendors JS -->
    <script src="{{url('frontend_style/js/vendor/modernizr-3.11.2.min.js')}}"></script>
    <script src="{{url('frontend_style/js/vendor/jquery-3.5.1.min.js')}}"></script>
    <script src="{{url('frontend_style/js/vendor/jquery-migrate-3.3.0.min.js')}}"></script>
    <script src="{{url('frontend_style/js/vendor/bootstrap.bundle.min.js')}}"></script>

    <!-- Plugins JS -->
    <script src="{{url('frontend_style/js/plugins/aos.min.js')}}"></script>
    <script src="{{url('frontend_style/js/plugins/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{url('frontend_style/js/plugins/jquery.counterup.min.js')}}"></script>
    <script src="{{url('frontend_style/js/plugins/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{url('frontend_style/js/plugins/vivus.min.js')}}"></script>
    <!-- <script src="frontend_style/js/vendor/vendor.min.js"></script>
    <script src="frontend_style/js/plugins/plugins.min.js"></script> -->

    <!-- Main Activation JS -->
    <script src="{{url('frontend_style/js/main.js')}}"></script>



</body>

</html>