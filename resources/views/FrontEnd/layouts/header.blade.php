
<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Fitness Fit</title>
    <meta name="robots" content="index, follow" />
    <meta name="description" content="Try out Gymate Gym Fitness Bootstrap 5 Template today because this awesome fitness related website template comes completely free of any cost.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="frontend_style/images/favicon.png">

    <!-- CSS
	============================================ -->

    <!-- Vendor CSS (Bootstrap & Icon Font) -->     
    <link rel="stylesheet" href="{{url('frontend_style/css/vendor/font-awesome-pro.min.css')}}">
    <link rel="stylesheet" href="{{url('frontend_style/css/vendor/pe-icon-7-stroke.css')}}">
    <!-- Plugins CSS (All Plugins Files) -->
    <link rel="stylesheet" href="{{url('frontend_style/css/plugins/swiper.min.css')}}">
    <link rel="stylesheet" href="{{url('frontend_style/css/plugins/animate.css')}}">
    <link rel="stylesheet" href="{{url('frontend_style/css/plugins/aos.min.css')}}">
    <link rel="stylesheet" href="{{url('frontend_style/css/plugins/magnific-popup.css')}}">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{url('frontend_style/css/style.css')}}">

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <!-- <link rel="stylesheet" href="frontend_style/css/vendor/vendor.min.css">
    <link rel="stylesheet" href="frontend_style/css/plugins/plugins.min.css">
    <link rel="stylesheet" href="frontend_style/css/style.min.css"> -->

</head>

<body>
    <div id="page" class="section">
        <!-- Header Section Start -->
        <div class="header-section header-transparent sticky-header header-fluid section">
            <div class="header-inner">
                <div class="container position-relative">
                    <div class="row justify-content-between align-items-center">

                        <!-- Header Logo Start -->
                        <div class="col-xl-2 col-auto">
                            <div class="header-logo">
                                <a href="index.html">
                                    <img class="dark-logo" src="frontend_style/images/logo/logo.png" alt="Fitness Logo">
                                    <img class="light-logo" src="frontend_style/images/logo/logo.png" alt="Fitness Logo">
                                </a>
                            </div>
                        </div>
                        <!-- Header Logo End -->

                     <!-- Header Main Menu Start -->
                     <div class="col d-none d-xl-block">
                            <div class="menu-column-area d-none d-xl-block position-static">
                                <nav class="site-main-menu">
                                    <ul>
                                        <li>
                                            <a href="#home"><span class="menu-text">Home</span></a>
                                        </li>
                                        <li class="has-children">
                                            <a href="#home"><span class="menu-text">Pages</span></a>
                                            <span class="menu-toggle"><i class="far fa-angle-down"></i></span>
                                            <ul class="sub-menu">
                                                <li><a href="#about"><span class="menu-text" >About</span></a></li>
                                                <li><a href="#EXPERIENCED_COACHES"><span class="menu-text">Our Team</span></a></li>
                                                <li><a href="#contact"><span class="menu-text">Contact</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#timetable"><span class="menu-text">Our Program</span></a>
                                        </li>
                                  
                                            
                                        <li>
                                            <a href="{{Route('bl')}}"><span class="menu-text">Admin Login</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- Header Main Menu End -->


                        <!-- Header Right Start -->
                        <div class="col-xl-2 col-auto d-flex align-items-center justify-content-end">
                            <!-- Header Search Start -->
                            <div class="header-search-area mr-xl-4 mr-0">

                                <!-- Header Login Start -->
                                <div class="header-search">
                                    <a href="javascript:void(0)" class="header-search-toggle"><i
                                            class="pe-7s-search pe-2x pe-va"></i></a>
                                </div>
                                <!-- Header Login End -->
                            </div>
                            <!-- Header Search End -->

                          
                                <div class="header-mini-cart">
                                    <div class="inner">

                                        <!-- Header Mini Cart Product Start -->
                                        <div class="mini-cart-products">
                                            <div class="mini-cart-product">
                                                <a href="#" class="thumb"><img
                                                        src="frontend_style/images/shop/shopping-cart/product-1.jpg" alt=""></a>
                                                <div class="content">
                                                    <a href="#" class="title">Online Student: Strategies for Effective
                                                        Learning</a>
                                                    <span class="quantity">1 x <span class="price">$67.00</span></span>
                                                </div>
                                                <a href="#" class="remove"><i class="far fa-times"></i></a>
                                            </div>
                                            <div class="mini-cart-product">
                                                <a href="#" class="thumb"><img
                                                        src="frontend_style/images/shop/shopping-cart/product-2.jpg" alt=""></a>
                                                <div class="content">
                                                    <a href="#" class="title">Principles of Business Administration</a>
                                                    <span class="quantity">1 x <span class="price">$52.00</span></span>
                                                </div>
                                                <a href="#" class="remove"><i class="far fa-times"></i></a>
                                            </div>
                                        </div>
                                        <!-- Header Mini Cart Product End -->

                                        <!-- Header Mini Cart Footer Start -->
                                        <div class="mini-cart-footer">
                                            <div class="mini-cart-total">
                                                <b>Total:</b>
                                                <span class="amount">$119.00</span>
                                            </div>
                                            <div class="mini-cart-buttons">
                                                <a href="shopping-cart.html"
                                                    class="btn btn-primary btn-hover-secondary">View Cart</a>
                                                <a href="checkout.html"
                                                    class="btn btn-primary btn-hover-secondary">Checkout</a>
                                            </div>
                                        </div>
                                        <!-- Header Mini Cart Footer End -->
                                    </div>
                                </div>
                                <!-- Header Mini Cart End -->
                            </div>
                            <!-- Header Cart End -->

                            <!-- Header Mobile Menu Toggle Start -->
                            <div class="header-mobile-menu-toggle d-xl-none ml-sm-2">
                                <button class="toggle">
                                    <i class="icon-top"></i>
                                    <i class="icon-middle"></i>
                                    <i class="icon-bottom"></i>
                                </button>
                            </div>
                            <!-- Header Mobile Menu Toggle End -->
                        </div>
                        <!-- Header Right End -->

                    </div>
                </div>
            </div>
        </div>
        <!-- Header Section End -->