@extends('frontend.layouts.main')
@section('content')


        <!-- Main Search Start -->
        <div class="main-search-active">
            <div class="sidebar-search-icon">
                <button class="search-close"><i class="fal fa-times"></i></button>
            </div>
            <div class="sidebar-search-input">
                <form action="#">
                    <div class="form-search">
                        <input id="search" class="input-text" value="" placeholder="" type="search">
                        <button>
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
                <p class="form-description">Hit enter to search or ESC to close</p>
            </div>
        </div>
        <!-- Main Search End -->

        <!-- Slider/Intro Section Start -->
        <div class="intro-slider-wrap section">
            <div class="intro-section section" data-bg-image="frontend_style/images/intro/intro1.jpg"  id ="home">

                <div class="container-fluid">
                    <div class="row row-cols-lg-1 row-cols-1">

                        <div class="col align-self-center">
                            <div class="intro-content text-center">
                                <span class="sub-title">Welcome to Richter CrossFit Center</span>
                                <h2 class="title">Work Hard EveryDay</h2>
                                <a href="#" class="btn btn-primary btn-hover-secondary btn-width-290-80">For Men</a>
                                <a href="#" class="btn btn-outline-white btn-hover-primary btn-width-290-80">For
                                    Women</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- Slider/Intro Section End -->

        <!-- Gym Categories Start -->
        <div class="section">
            <div class="container-fluid p-0">
                <div class="row row-cols-xl-4 row-cols-lg-2 row-cols-sm-2 row-cols-1 g-0">

                    <!-- Single Gym Categories Start -->
                    <div class="col" data-aos="fade-right">
                        <div class="gym-cate">
                            <div class="cat-image">
                                <a class="image" href="#"><img src="frontend_style/images/gym-cate/cate-1.jpg"
                                        alt="Categories"></a>
                            </div>
                            <div class="cate-content">
                                <div class="cate-inner">
                                    <h3 class="title">CARDIO</h3>
                                    <a href="#" class="btn btn-light btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Gym Categories End -->

                    <!-- Single Gym Categories Start -->
                    <div class="col" data-aos="fade-up">
                        <div class="gym-cate">
                            <div class="cat-image">
                                <a class="image" href="#"><img src="frontend_style/images/gym-cate/cate-2.jpg"
                                        alt="Categories"></a>
                            </div>
                            <div class="cate-content">
                                <div class="cate-inner">
                                    <h3 class="title">BODY BUILD</h3>
                                    <a href="#" class="btn btn-light btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Gym Categories End -->

                    <!-- Single Gym Categories Start -->
                    <div class="col" data-aos="fade-down">
                        <div class="gym-cate">
                            <div class="cat-image">
                                <a class="image" href="#"><img src="frontend_style/images/gym-cate/cate-3.jpg"
                                        alt="Categories"></a>
                            </div>
                            <div class="cate-content">
                                <div class="cate-inner">
                                    <h3 class="title">BOXING</h3>
                                    <a href="#" class="btn btn-light btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Gym Categories End -->

                    <!-- Single Gym Categories Start -->
                    <div class="col" data-aos="fade-left">
                        <div class="gym-cate">
                            <div class="cat-image">
                                <a class="image" href="#"><img src="frontend_style/images/gym-cate/cate-4.jpg"
                                        alt="Categories"></a>
                            </div>
                            <div class="cate-content">
                                <div class="cate-inner">
                                    <h3 class="title">YOGA</h3>
                                    <a href="#" class="btn btn-light btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Gym Categories End -->

                </div>
            </div>
        </div>
        <!-- Gym Categories End -->

        <!-- About Section Start -->
        <div class="section section-padding-t100-b140 section-fluid" >
            <div class="container">
                <div class="stroke-text">
                    <h2 class="heading-title">FITYOUR <br> DAY</h2>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-7" data-aos="fade-up">
                        <!-- About Image Start -->
                        <div class="about-image">
                            <img src="frontend_style/images/about/about-2.png" alt="">
                        </div>
                        <!-- About Image End -->
                    </div>
                    <div class="col-lg-5" data-aos="fade-up">
                        <!-- About Content Start -->
                        <div class="about-content">
                            <h3 class="title">Improve Yourself Everyday</h3>
                            <p>Vestibulum sed lectus massa. Donec egestas, diam sed ultrices hendrerit, est justo
                                gravida eros, a mollis ipsum justo dapibus lacus. Pellentesque dui libero, ultricies</p>
                            <a href="#" class="btn btn-primary btn-hover-dark">START NOW</a>
                        </div>
                        <!-- About Content End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- About Section End -->

        <!-- Coaches Section Start -->
        <div class="section section-padding-t100-b140 section-fluid-240 bg-linear-black" id="EXPERIENCED_COACHES">
            <div class="container">

                <!-- Section Title Start -->
                <div class="section-title color-light text-center" data-aos="fade-up">
                    <h2 class="title">EXPERIENCED COACHES</h2>
                    <p class="sub-title">Phasellus nulla mauris, imperdiet a augue a</p>
                </div>
                <!-- Section Title End -->

                <div class="row row-cols-lg-3 row-cols-sm-2 row-cols-1 mb-n6">

                    <div class="col mb-6" data-aos="fade-right">
                        <!-- Single Coach Start -->
                        <div class="single-coach">
                            <div class="thumbnial">
                                <a class="image" href="#"><img src="frontend_style/images/team/team-1.jpg" alt=""></a>
                                <div class="inner-block">
                                    <div class="slider-top-right"></div>
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title">Owen Cannon</h3>
                                <span class="designation">Gym Coacher</span>
                            </div>
                        </div>
                        <!-- Single Coach End -->
                    </div>

                    <div class="col mb-6" data-aos="fade-up">
                        <!-- Single Coach Start -->
                        <div class="single-coach">
                            <div class="thumbnial">
                                <a class="image" href="#"><img src="frontend_style/images/team/team-2.jpg" alt=""></a>
                                <div class="inner-block">
                                    <div class="slider-top-right"></div>
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title">Margaret Guzman</h3>
                                <span class="designation">Yoga Coacher</span>
                            </div>
                        </div>
                        <!-- Single Coach End -->
                    </div>

                    <div class="col mb-6" data-aos="fade-left">
                        <!-- Single Coach Start -->
                        <div class="single-coach">
                            <div class="thumbnial">
                                <a class="image" href="#"><img src="frontend_style/images/team/team-3.jpg" alt=""></a>
                                <div class="inner-block">
                                    <div class="slider-top-right"></div>
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title">Minnie Sharp</h3>
                                <span class="designation">Dancesport Coacher</span>
                            </div>
                        </div>
                        <!-- Single Coach End -->
                    </div>

                </div>
            </div>
        </div>
        <!-- Coaches Section End -->

        <!-- Event Section Start -->
        <div class="section section-padding-t100-b115 section-fluid">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section-title text-center" data-aos="fade-up">
                    <h2 class="title">CLASS EVENTS</h2>
                    <p class="sub-title">Phasellus nulla mauris, imperdiet a augue a</p>
                </div>
                <!-- Section Title End -->
                <div class="event-slider swiper-container" data-aos="fade-up" data-aos-delay="300">

                    <div class="swiper-wrapper">

                        <div class="swiper-slide">
                            <!-- Single Event Start -->
                            <div class="single-event">
                                <div class="event-head">
                                    <h3 class="title">GRADIO</h3>
                                    <span class="date">01-27-2020</span>
                                </div>
                                <div class="desc">
                                    <p>Vestibulum sed lectus massa. Donec egestas, diam sed ultrices hendrerit, est
                                        justo gravida eros, a mollis ips</p>
                                </div>
                                <div class="action">
                                    <a href="#" class="btn btn-outline-secondary btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                            <!-- Single Event End -->
                        </div>

                        <div class="swiper-slide">
                            <!-- Single Event Start -->
                            <div class="single-event">
                                <div class="event-head">
                                    <h3 class="title">CROSSFIT</h3>
                                    <span class="date">01-27-2020</span>
                                </div>
                                <div class="desc">
                                    <p>Vestibulum sed lectus massa. Donec egestas, diam sed ultrices hendrerit, est
                                        justo gravida eros, a mollis ips</p>
                                </div>
                                <div class="action">
                                    <a href="#" class="btn btn-outline-secondary btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                            <!-- Single Event End -->
                        </div>

                        <div class="swiper-slide">
                            <!-- Single Event Start -->
                            <div class="single-event">
                                <div class="event-head">
                                    <h3 class="title">POWERLIFTING</h3>
                                    <span class="date">01-27-2020</span>
                                </div>
                                <div class="desc">
                                    <p>Vestibulum sed lectus massa. Donec egestas, diam sed ultrices hendrerit, est
                                        justo gravida eros, a mollis ips</p>
                                </div>
                                <div class="action">
                                    <a href="#" class="btn btn-outline-secondary btn-hover-primary">START NOW</a>
                                </div>
                            </div>
                            <!-- Single Event End -->
                        </div>

                    </div>

                    <div class="col">

                    </div>

                </div>
            </div>
        </div>
        <!-- Event Section End -->

        <!-- Video Section Start -->
        <div class="section section-padding-t185-b150 section-fluid" data-bg-image="frontend_style/images/bg/video-bg.jpg">
            <div class="container">
                <div class="video-stroke-text">
                    <h2 class="heading-title">GO HARD <br> OR GO HOME</h2>
                </div>
                <div class="play-btn-area">
                    <a class="play-btn icon video-popup" href="https://www.youtube.com/watch?v=eS9Qm4AOOBY"><i
                            class="fal fa-play"></i></a>
                </div>
            </div>
        </div>
        <!-- Video Section End -->

        <!-- Calculate Section Start  -->
        <div class="section section-fluid-240">
            <div class="container">
                <div class="calculate-box">
                    <!-- Section Title Start -->
                    <div class="section-title text-center" data-aos="fade-up">
                        <h2 class="title">CALCULATE YOUR BMI</h2>
                        <p>Your BMR calculator generates the number of calories your body burns per day at rest. Your
                            BMR with activity factor is the number of calories your body burns per day based on the
                            activity factor you selected.</p>
                    </div>
                    <!-- Section Title End -->
                    <div class="calculate-form" data-aos="fade-up" data-aos-delay="300">
                        <form action="#">
                            <div class="row mb-n6">
                                <div class="col-lg-6 mb-6">
                                    <input type="text" placeholder="Height / cm" name="height">
                                </div>
                                <div class="col-lg-6 mb-6">
                                    <input type="text" placeholder="Weight / kg" name="weight">
                                </div>
                                <div class="col-lg-6 mb-6">
                                    <input type="text" placeholder="Age" name="age">
                                </div>
                                <div class="col-lg-6 mb-6">
                                    <input type="text" placeholder="Sex" name="sex">
                                </div>
                                <div class="col-12 mb-6">
                                    <select>
                                        <option value="default">Select an activity factor:</option>
                                        <option value="little">Little or no Exercise / desk job</option>
                                        <option value="light">Light exercise / sports 1 – 3 days/ week</option>
                                        <option value="moderate">Moderate Exercise, sports 3 – 5 days / week</option>
                                        <option value="heavy">Heavy Exercise / sports 6 – 7 days / week</option>
                                    </select>
                                </div>
                                <div class="col-12 text-center mb-6">
                                    <button class="btn btn-primary btn-hover-dark btn-width-200-60">CALCULATE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Calculate Section End  -->

        <!-- Banner Section Start -->
        <div class="section">
            <div class="container-fulid">
                <div class="row row-cols-lg-2 row-cols-1 g-0">

                    <div class="col" data-aos="fade-up">
                        <!-- Single Banner Start -->
                        <div class="single-banner">
                            <div class="thumbnail">
                                <a class="image" href="#"><img src="frontend_style/images/banner/banner-1.jpg" alt=""></a>
                                <div class="inner-block">
                                    <div class="slider-top-right"></div>
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title">COVID-19 <br> ONLINE FITNESS</h3>
                                <a href="#" class="btn btn-primary btn-hover-dark">START NOW</a>
                            </div>
                        </div>
                        <!-- Single Banner End -->
                    </div>

                    <div class="col" data-aos="fade-up" data-aos-delay="300">
                        <!-- Single Banner Start -->
                        <div class="single-banner">
                            <div class="thumbnail">
                                <a class="image" href="#"><img src="frontend_style/images/banner/banner-2.jpg" alt=""></a>
                                <div class="inner-block">
                                    <div class="slider-top-right"></div>
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title">COMPORTABLE SOLUTION</h3>
                                <a href="#" class="btn btn-primary btn-hover-dark">START NOW</a>
                            </div>
                        </div>
                        <!-- Single Banner End -->
                    </div>

                </div>
            </div>
        </div>
        <!-- Banner Section End -->

        <!-- Testimonial Section Start -->
        <div class="section section-padding-t120-b100 section-fluid-240">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <!-- Section Title Start -->
                        <div class="section-title text-left mb-lg-0 mb-40" data-aos="fade-up">
                            <h2 class="title">OUR TESTIMONIAL</h2>
                        </div>
                        <!-- Section Title End -->
                    </div>
                    <div class="col-lg-8">
                        <!--Testimonial Slider Start -->
                        <div class="testimonial-slider swiper-container" data-aos="fade-up" data-aos-delay="300">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="testimonial">
                                        <div class="qute-icon">
                                            <img src="frontend_style/images/icon/qute.svg" alt="qute">
                                        </div>
                                        <div class="testimonial-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="content">
                                            <p>Vestibulum sed lectus massa. Donec egestas, diam sed ultrices hendrerit,
                                                est justo gravida eros, a mollis ipsum justo dapibus lacus. Pellentesque
                                                dui libero, ultricies</p>
                                        </div>
                                        <div class="author-info">
                                            <div class="image">
                                                <img src="frontend_style/images/testimonial/testimonial-1.png" alt="">
                                            </div>
                                            <div class="cite">
                                                <p class="name">Bobby Hughes</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <!--Testimonial Slider End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial Section End -->

        <!-- Page Title Section Start -->
        <div class="page-title-section section" data-overlay="0.7" data-bg-image="frontend_style/images/bg/breadcrumb-about.jpg" id="about" >
            <div class="page-title pt-lg-10 pt-10">
                <div class="container">
                    <h1 class="title" >About Us</h1>
                </div>
            </div>
        </div>
        <!-- Page Title Section End -->

        <!-- Building Fitness Section Start -->
        <div class="section section-fluid section-padding-t180-b210">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-5 col-lg-12">
                        <div class="building-fitness-content">
                            <!-- Section Title Two Start -->
                            <div class="section-title-two text-xl-left text-center mb-6" data-aos="fade-up">
                                <span class="sub-title">WHAT WE DO</span>
                                <h2 class="title">Building confidence. <br> Building fitness</h2>
                                <p class="desc m-xl-0 m-auto">
                                    Vestibulum sed lectus massa. Donec egestas, diam sed ultrices hendrerit, est justo gravida eros, a mollis ipsum justo dapibus lacus. Pellentesque
                                </p>
                            </div>
                            <!-- Section Title Two End -->

                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12">
                        <div class="row row-cols-lg-2 row-cols-sm-2 row-cols-1 mb-n14">
                            <div class="col mb-14" data-aos="fade-up">
                                <!-- Single Icon Box Start -->
                                <div class="icon-box text-center">
                                    <div class="icon" data-vivus-hover>
                                        <img class="svgInject" src="frontend_style/images/icon/icon-box2.svg" alt="">
                                    </div>
                                    <div class="content">
                                        <h3 class="title">GROUP FITNESS</h3>
                                        <div class="desc">
                                            <p>Phasellus nulla mauris, imperdiet</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Icon Box End -->
                            </div>
                            <div class="col mb-14" data-aos="fade-up" data-aos-delay="300">
                                <!-- Single Icon Box Start -->
                                <div class="icon-box text-center">
                                    <div class="icon" data-vivus-hover>
                                        <img class="svgInject" src="frontend_style/images/icon/icon-box3.svg" alt="">
                                    </div>
                                    <div class="content">
                                        <h3 class="title">GROUP FITNESS</h3>
                                        <div class="desc">
                                            <p>Phasellus nulla mauris, imperdiet</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Icon Box End -->
                            </div>
                            <div class="col mb-14" data-aos="fade-up" data-aos-delay="600">
                                <!-- Single Icon Box Start -->
                                <div class="icon-box text-center">
                                    <div class="icon" data-vivus-hover>
                                        <img class="svgInject" src="frontend_style/images/icon/icon-box1.svg" alt="">
                                    </div>
                                    <div class="content">
                                        <h3 class="title">GROUP FITNESS</h3>
                                        <div class="desc">
                                            <p>Phasellus nulla mauris, imperdiet</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Icon Box End -->
                            </div>
                            <div class="col mb-14" data-aos="fade-up" data-aos-delay="900">
                                <!-- Single Icon Box Start -->
                                <div class="icon-box text-center">
                                    <div class="icon" data-vivus-hover>
                                        <img class="svgInject" src="frontend_style/images/icon/icon-box4.svg" alt="">
                                    </div>
                                    <div class="content">
                                        <h3 class="title">GROUP FITNESS</h3>
                                        <div class="desc">
                                            <p>Phasellus nulla mauris, imperdiet</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Icon Box End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Building Fitness Section End -->

        <!-- Fit Section Start -->
        <div class="section">
            <div class="container-fluid px-0">
                <div class="row row-cols-lg-2 row-cols-1 g-0">
                    <div class="col" data-aos="fade-up">
                        <!-- Fit Image Start -->
                        <div class="fit-image">
                            <img src="frontend_style/images/fit/fit-bg.jpg" alt="fit">
                        </div>
                        <!-- Fit Image End -->
                    </div>
                    <div class="col" data-aos="fade-up" data-aos-delay="300">
                        <!-- Fit Content Start -->
                        <div class="fit-content">
                            <div class="fit-inner">
                                <!-- Section Title Two Start -->
                                <div class="section-title-two color-light">
                                    <span class="sub-title">WHAT WE COMMITED</span>
                                    <h2 class="title">Commit to be Fit</h2>
                                    <p class="desc">Suspendisse eu odio consequat, lacinia tellus at, molestie orci. Suspendisse eu eros condimentum, tincidunt libero</p>
                                </div>
                                <!-- Section Title Two End -->
                                <ul class="fitness-list">
                                    <li class="item">
                                        <div class="icon">
                                            <i class="fal fa-check-circle"></i>
                                        </div>
                                        <div class="text">Tia tellus at, molestie oruspendis</div>
                                    </li>
                                    <li class="item">
                                        <div class="icon">
                                            <i class="fal fa-check-circle"></i>
                                        </div>
                                        <div class="text">Fermentum non massa nec</div>
                                    </li>
                                    <li class="item">
                                        <div class="icon">
                                            <i class="fal fa-check-circle"></i>
                                        </div>
                                        <div class="text">Vestibulum vulputate felis non</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Fit Content End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Fit Section End -->

        <!-- Brand Section Start -->
        <div class="section section-padding-t120-b140">
            <div class="container">
                <!-- Section Title Two Start -->
                <div class="section-title-two text-center" data-aos="fade-up">
                    <span class="sub-title">TRUSTED CLIENT</span>
                    <h2 class="title">Everyone in life needs balance.</h2>
                    <p class="desc m-auto">Suspendisse eu odio consequat, lacinia tellus at, molestie orci. Suspendisse eu eros condimentum, tincidunt libero</p>
                </div>
                <!-- Section Title Two End -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="brand-wrapper" data-aos="fade-up" data-aos-delay="300">
                            <div class="brand-list">
                                <div class="brand-carousel swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-01.png" alt="logo image">
                                            </a>
                                        </div>

                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-02.png" alt="logo image">
                                            </a>
                                        </div>

                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-03.png" alt="logo image">
                                            </a>
                                        </div>

                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-04.png" alt="logo image">
                                            </a>
                                        </div>

                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-01.png" alt="logo image">
                                            </a>
                                        </div>

                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-03.png" alt="logo image">
                                            </a>
                                        </div>

                                        <div class="swiper-slide brand">
                                            <a href="#">
                                                <img src="frontend_style/images/brand/client-logo-03.png" alt="logo image">
                                            </a>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brand Section End -->

        <!-- Funfact Section Start -->
        <div class="section section-padding-200" data-bg-image="frontend_style/images/bg/funfact-bg.jpg">
            <div class="container" data-aos="fade-up">
                <div class="row row-cols-lg-3 row-cols-sm-2 row-cols-1 mb-n6">
                    <!-- Funfact Start -->
                    <div class="col mb-6">
                        <div class="funfact text-center">
                            <div class="number"><span class="counter">584</span></div>
                            <h6 class="text">Dare to be great</h6>
                        </div>
                    </div>
                    <!-- Funfact End -->

                    <!-- Funfact Start -->
                    <div class="col mb-6">
                        <div class="funfact text-center">
                            <div class="number"><span class="counter">1090</span></div>
                            <h6 class="text">ibero, ultricies</h6>
                        </div>
                    </div>
                    <!-- Funfact End -->

                    <!-- Funfact Start -->
                    <div class="col mb-6">
                        <div class="funfact text-center">
                            <div class="number"><span class="counter">253</span></div>
                            <h6 class="text">Pellentesque dui</h6>
                        </div>
                    </div>
                    <!-- Funfact End -->
                </div>
            </div>
        </div>
        <!-- Funfact Section End -->

        <!-- Skill Section Start -->
        <div class="section section-padding-t180-b180">
            <div class="container">
                <!-- Section Title Two Start -->
                <div class="section-title-two text-center" data-aos="fade-up">
                    <span class="sub-title">OUR SPECIAL SKILLS</span>
                    <h2 class="title">Don't stop till you drop!</h2>
                    <p class="desc m-auto">Suspendisse eu odio consequat, lacinia tellus at, molestie orci. Suspendisse eu eros condimentum, tincidunt lib</p>
                </div>
                <!-- Section Title Two End -->
                
        <!-- Page Title Section Start -->
        <div class="page-title-section section" data-bg-image="frontend_style/images/bg/breadcrumb-program.jpg" id="timetable">
            <div class="page-title pt-lg-10 pt-10">
                <div class="container">
                    <h1 class="title">WORKING HOURS AND CLASSES</h1>
                </div>
            </div>
        </div>
        <!-- Page Title Section End -->
      <!-- Program Section Start -->
      <div class="section section-padding-t100-b200 section-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="program-tab" data-aos="fade-up">
                            <ul class="nav justify-content-center">
                                <li>
                                    <a class="active" data-toggle="tab" href="#all">ALL EVENTS</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#cardio">CARDIO</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#crossfit">CROSSFIT</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#gym">GYM</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#powerlifting">POWERLIFTING</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" data-aos="fade-up" data-aos-delay="300">
                            <div class="tab-pane fade show active" id="all">
                                <div class="all-program-routine table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="heading-row">
                                                <th></th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wednesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                                <th>Sunday</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="row-one">
                                                <td class="tt-hours-column">6:00 - 07:00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">Open Gym</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event active" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                    <span class="table-badge">GYM</span>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-two">
                                                <td class="tt-hours-column">07:00 - 08:00</td>
                                            </tr>
                                            <tr class="row-three">
                                                <td class="tt-hours-column">08:00 - 09:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-four">
                                                <td class="tt-hours-column">10:00 - 11:00</td>
                                            </tr>
                                            <tr class="row-five">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-six">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="5">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                    <br>
                                                    <br>
                                                    <a class="event-header" href="#">CROSSFIT CLASS</a>
                                                    <div class="before-hour-text">Harold Kim</div>

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-seven">
                                                <td class="tt-hours-column">12:00 - 13:00</td>
                                            </tr>
                                            <tr class="row-eight">
                                                <td class="tt-hours-column">13:00 - 14:00</td>
                                                <td class="tt-single-event" rowspan="3">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                    <div class="before-hour-text">Madison Fren</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-nine">
                                                <td class="tt-hours-column">15:00 - 16:00</td>
                                            </tr>
                                            <tr class="row-ten">
                                                <td class="tt-hours-column">16:00 - 17:00</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-eleven">
                                                <td class="tt-hours-column">06.00 - 07.00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                            </tr>
                                            <tr class="row-twelve">
                                                <td class="tt-hours-column">17:00 - 18:00</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="cardio">
                                <div class="all-program-routine table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="heading-row">
                                                <th></th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wednesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                                <th>Sunday</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="row-one">
                                                <td class="tt-hours-column">6:00 - 07:00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">Open Gym</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event active" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                    <span class="table-badge">GYM</span>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-two">
                                                <td class="tt-hours-column">07:00 - 08:00</td>
                                            </tr>
                                            <tr class="row-three">
                                                <td class="tt-hours-column">08:00 - 09:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-four">
                                                <td class="tt-hours-column">10:00 - 11:00</td>
                                            </tr>
                                            <tr class="row-five">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-six">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="5">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                    <br>
                                                    <br>
                                                    <a class="event-header" href="#">CROSSFIT CLASS</a>
                                                    <div class="before-hour-text">Harold Kim</div>

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-seven">
                                                <td class="tt-hours-column">12:00 - 13:00</td>
                                            </tr>
                                            <tr class="row-eight">
                                                <td class="tt-hours-column">13:00 - 14:00</td>
                                                <td class="tt-single-event" rowspan="3">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                    <div class="before-hour-text">Madison Fren</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-nine">
                                                <td class="tt-hours-column">15:00 - 16:00</td>
                                            </tr>
                                            <tr class="row-ten">
                                                <td class="tt-hours-column">16:00 - 17:00</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-eleven">
                                                <td class="tt-hours-column">06.00 - 07.00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                            </tr>
                                            <tr class="row-twelve">
                                                <td class="tt-hours-column">17:00 - 18:00</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="crossfit">
                                <div class="all-program-routine table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="heading-row">
                                                <th></th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wednesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                                <th>Sunday</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="row-one">
                                                <td class="tt-hours-column">6:00 - 07:00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">Open Gym</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event active" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                    <span class="table-badge">GYM</span>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-two">
                                                <td class="tt-hours-column">07:00 - 08:00</td>
                                            </tr>
                                            <tr class="row-three">
                                                <td class="tt-hours-column">08:00 - 09:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-four">
                                                <td class="tt-hours-column">10:00 - 11:00</td>
                                            </tr>
                                            <tr class="row-five">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-six">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="5">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                    <br>
                                                    <br>
                                                    <a class="event-header" href="#">CROSSFIT CLASS</a>
                                                    <div class="before-hour-text">Harold Kim</div>

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-seven">
                                                <td class="tt-hours-column">12:00 - 13:00</td>
                                            </tr>
                                            <tr class="row-eight">
                                                <td class="tt-hours-column">13:00 - 14:00</td>
                                                <td class="tt-single-event" rowspan="3">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                    <div class="before-hour-text">Madison Fren</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-nine">
                                                <td class="tt-hours-column">15:00 - 16:00</td>
                                            </tr>
                                            <tr class="row-ten">
                                                <td class="tt-hours-column">16:00 - 17:00</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-eleven">
                                                <td class="tt-hours-column">06.00 - 07.00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                            </tr>
                                            <tr class="row-twelve">
                                                <td class="tt-hours-column">17:00 - 18:00</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="gym">
                                <div class="all-program-routine table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="heading-row">
                                                <th></th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wednesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                                <th>Sunday</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="row-one">
                                                <td class="tt-hours-column">6:00 - 07:00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">Open Gym</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event active" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                    <span class="table-badge">GYM</span>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-two">
                                                <td class="tt-hours-column">07:00 - 08:00</td>
                                            </tr>
                                            <tr class="row-three">
                                                <td class="tt-hours-column">08:00 - 09:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-four">
                                                <td class="tt-hours-column">10:00 - 11:00</td>
                                            </tr>
                                            <tr class="row-five">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-six">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="5">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                    <br>
                                                    <br>
                                                    <a class="event-header" href="#">CROSSFIT CLASS</a>
                                                    <div class="before-hour-text">Harold Kim</div>

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-seven">
                                                <td class="tt-hours-column">12:00 - 13:00</td>
                                            </tr>
                                            <tr class="row-eight">
                                                <td class="tt-hours-column">13:00 - 14:00</td>
                                                <td class="tt-single-event" rowspan="3">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                    <div class="before-hour-text">Madison Fren</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-nine">
                                                <td class="tt-hours-column">15:00 - 16:00</td>
                                            </tr>
                                            <tr class="row-ten">
                                                <td class="tt-hours-column">16:00 - 17:00</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-eleven">
                                                <td class="tt-hours-column">06.00 - 07.00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                            </tr>
                                            <tr class="row-twelve">
                                                <td class="tt-hours-column">17:00 - 18:00</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="powerlifting">
                                <div class="all-program-routine table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="heading-row">
                                                <th></th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wednesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                                <th>Sunday</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="row-one">
                                                <td class="tt-hours-column">6:00 - 07:00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">Open Gym</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event active" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                    <span class="table-badge">GYM</span>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-two">
                                                <td class="tt-hours-column">07:00 - 08:00</td>
                                            </tr>
                                            <tr class="row-three">
                                                <td class="tt-hours-column">08:00 - 09:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-four">
                                                <td class="tt-hours-column">10:00 - 11:00</td>
                                            </tr>
                                            <tr class="row-five">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-six">
                                                <td class="tt-hours-column">11:00 - 12:00</td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="5">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Harold Kim</div>
                                                    <br>
                                                    <br>
                                                    <a class="event-header" href="#">CROSSFIT CLASS</a>
                                                    <div class="before-hour-text">Harold Kim</div>

                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                            </tr>
                                            <tr class="row-seven">
                                                <td class="tt-hours-column">12:00 - 13:00</td>
                                            </tr>
                                            <tr class="row-eight">
                                                <td class="tt-hours-column">13:00 - 14:00</td>
                                                <td class="tt-single-event" rowspan="3">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">BODY CLASS</a>
                                                    <div class="before-hour-text">Don Ortega</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                    <div class="before-hour-text">Madison Fren</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">POWER LIFTING</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                            </tr>
                                            <tr class="row-nine">
                                                <td class="tt-hours-column">15:00 - 16:00</td>
                                            </tr>
                                            <tr class="row-ten">
                                                <td class="tt-hours-column">16:00 - 17:00</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="row-eleven">
                                                <td class="tt-hours-column">06.00 - 07.00</td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">

                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">YOGA CLASS</a>
                                                    <div class="before-hour-text">Fanny Davis</div>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">CARDIO BURN</a>
                                                </td>
                                                <td class="tt-single-event" rowspan="2">
                                                    <a class="event-header" href="#">OPEN GYM</a>
                                                </td>
                                            </tr>
                                            <tr class="row-twelve">
                                                <td class="tt-hours-column">17:00 - 18:00</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Program Section End -->

             <!-- Page Title Section Start -->
        <div class="page-title-section section" data-bg-image="frontend_style/images/bg/breadcrumb-contact.jpg" id="contact">
            <div class="page-title pt-lg-10 pt-10">
                <div class="container">
                    <h1 class="title">CONTACT US</h1>
                </div>
            </div>
        </div>
        <!-- Page Title Section End -->      
        <!-- Contact Us Section Start -->
        <div class="section">
            <div class="container-fluid px-0">
                <div class="row row-cols-lg-2 row-cols-1 g-0">
                    <div class="col" data-aos="fade-up">
                        <!-- Google Map Start -->
                        <div class="contact-map-area">
                            <iframe class="contact-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2136.986005919501!2d-73.9685579655238!3d40.75862446708152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258e4a1c884e5%3A0x24fe1071086b36d5!2sThe%20Atrium!5e0!3m2!1sen!2sbd!4v1585132512970!5m2!1sen!2sbd" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                        <!-- Google Map End -->
                    </div>
                    <div class="col" data-aos="fade-up" data-aos="fade-up" data-aos-delay="300">
                        <div class="contact-form-wrapper">
                            <div class="contact-form">
                                <form id="contact-form" action="https://htmlmail.hasthemes.com/shohel/richter-fitness/mail.php">
                                    <!-- Single Input Start -->
                                    <div class="single-input mb-6">
                                        <input type="text" name="name" placeholder="Name *">
                                    </div>
                                    <!-- Single Input End -->
                                    <!-- Single Input Start -->
                                    <div class="single-input mb-6">
                                        <input type="email" name="email" placeholder="Your Email *">
                                    </div>
                                    <!-- Single Input End -->
                                   
                                    <!-- Single Input Start -->
                                    <div class="single-input mb-6">
                                        <select name="plan">
                                            <option value="Your Idea / plan">Your Idea / plan</option>
                                            <option value="plane-a">Plane A</option>
                                            <option value="plane-b">Plane B</option>
                                            <option value="plane-c">Plane C</option>
                                            <option value="plane-d">Plane D</option>
                                            <option value="plane-e">Plane E</option>
                                        </select>
                                    </div>
                                    <!-- Single Input End -->
                                    <!-- Single Input Start -->
                                    <div class="single-input mb-6">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>
                                    <!-- Single Input End -->
                                    <!-- Single Input Start -->
                                    <div class="single-input mb-6">
                                        <button class="btn btn-primary btn-hover-dark w-100 btn-height-80">SEND MESSAGE</button>
                                    </div>
                                    <!-- Single Input End -->
                                </form>
                                <p class="form-messege"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Us Section End -->

              
       @endsection