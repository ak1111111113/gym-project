<?php

use Illuminate\Support\Facades\Route;
use app\http\Controller\formcontroller;


Route::get('/', function () {
    return view('welcome');
});

Route::get('', function () {
    return view('FrontEnd.index');
});

Route::get('/backend',function (){
    return view('BackEnd.index');
});


Route::get('/login',function (){
    return view('BackEnd.login');
})->name('bl');

Route::POST('submit',[formcontroller::class,'datasave']);